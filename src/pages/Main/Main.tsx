import { Hero, About, Footer } from 'components';
import { useStore } from 'effector-react';
import { useTitle } from 'shared';
import { ThemeStore } from '../../features/dark-mode/models';

export const Main = () => {
  const title = useTitle('Main Page');
  const theme = useStore(ThemeStore);
  return (
    <>
      <Hero />
      <About />
      <Footer />
    </>
  );
};
