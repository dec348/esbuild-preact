/* eslint-disable react/jsx-props-no-spreading */
import Router, { Route } from 'preact-router';
import { Main } from './Main';

const routes = [
  { path: '/', component: () => <Main /> },
];
export const Routing = () => (
  <Router>
    {routes.map((_) => <Route {..._} />)}
  </Router>
);
