import { Routing } from 'pages';
import { Header } from './Header';

export const App = () => {
  const links = [{ name: 'About' }, { name: 'Services' }, { name: 'Case Studios' }, { name: 'Careers' }, { name: 'Blog' }];
  return (
    <>
      <Header links={links} />
      <Routing />
    </>
  );
};
