/* eslint-disable jsx-a11y/anchor-is-valid */
import { Link } from 'preact-router';
import { useState } from 'hooks';
import objstr from 'obj-str';

interface Links {name: string}
interface HeaderProps { links: Links[] }

export const Header = ({ links }: HeaderProps) => {
  const [isBurgerOpen, setIsBurgerOpen] = useState(false);
  const burgerClickHandler = () => setIsBurgerOpen(!isBurgerOpen);

  return (
    <header class="relative">
      <div class="container">
        <nav class="flex justify-between items-center pt-[26px]">
          <a href="#"><img src="./assets/logo.svg" alt="" /></a>
          <ul class="flex gap-12 md:hidden ">
            {links.map(({ name }) => (
              <li>
                <Link class="font-medium text-xl text-lightblack hover:text-darkred transition-colors lg:text-base" href="#">{name}</Link>
              </li>
            ))}
          </ul>
          <ul class={objstr({ 'absolute left-6 right-6 top-24 bg-lightred  flex-col items-center': 1, hidden: !isBurgerOpen })}>
            {links.map(({ name }) => <li><Link class="block py-4 text-white" href="#">{name}</Link></li>)}
          </ul>
          <button class={objstr({ 'burger hidden md:block': 1, active: isBurgerOpen })} onClick={burgerClickHandler} type="button">
            <span class="burger-line top-line dark:bg-white" />
            <span class="burger-line mid-line dark:bg-white" />
            <span class="burger-line bottom-line dark:bg-white" />
          </button>
        </nav>
      </div>
    </header>
  );
};
