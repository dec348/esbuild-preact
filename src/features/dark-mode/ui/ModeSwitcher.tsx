import { switchTheme } from '../models';

export const ModeSwitcher = () => {
  const themeSwitchHandler = () => switchTheme();
  return (
    <div>
      <button class="btnRed absolute right-2" type="button" onClick={themeSwitchHandler}>mode</button>
    </div>
  );
};
