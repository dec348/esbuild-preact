import { createEvent, createStore } from 'effector';

type Theme = 'light' | 'dark';

const ThemeStore = createStore<Theme>('light');

const switchTheme = createEvent();

ThemeStore.on(switchTheme, () => (ThemeStore.getState() === 'light' ? 'dark' as Theme : 'light' as Theme));

export { Theme, ThemeStore, switchTheme };
