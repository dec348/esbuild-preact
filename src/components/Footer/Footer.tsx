export const Footer = () => (
  <footer class="bg-darkgray py-20 md:py-10">
    <div class="container">
      <div class="flex gap-40 md:flex-col md:gap-10">
        <div class="max-w-xs md:max-w-full">
          <a href="#">
            <img src="" alt="" />
          </a>
          <p class="mt-[18px] text-white text-xl">
            Sign up for our newsletter to get latest news from us
          </p>
          <input class="mt-3 bg-[#454545] flex w-full py-3 px-4 rounded-xl border-[1px] border-[#595959] text-white focus:outline-none focus:border-lightred placeholder:italic" type="text" placeholder="Enter your E-Mail" />
          <button class="redBtn" type="button">
            Submit
          </button>
        </div>
        <div class="">
          <div class="text-lightred text-xl font-medium">
            Jump to
          </div>
          <ul class="mt-2">
            <li class="mt-3"><a class="text-white text-xl font-medium" href="">About</a></li>
            <li class="mt-3"><a class="text-white text-xl font-medium" href="">Contact</a></li>
            <li class="mt-3"><a class="text-white text-xl font-medium" href="">Services</a></li>
            <li class="mt-3"><a class="text-white text-xl font-medium" href="">Case Studies</a></li>
            <li class="mt-3"><a class="text-white text-xl font-medium" href="">Blog</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>
);
