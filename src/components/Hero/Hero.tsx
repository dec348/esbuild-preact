/* eslint-disable jsx-a11y/anchor-is-valid */
export const Hero = () => (
  <section class="hero">
    <div class="container">
      <h1 class="pt-24 font-black text-5xl max-w-2xl text-center mx-auto leading-snug
        lg:text-4xl lg:pt-16 md:text-3xl md:pt-12 md:w-full sm:pt-10 sm:text-2xl"
      >
        Grow your buisness. Get. More. Leads

      </h1>
      <p class="text-xl tracking-tight text-center max-w-xl mx-auto mt-[18px] md:text-lg md:mt-4 sm:mt-2 sm:text-base">

        Wanna get serious into digital marketing?
        Then you need leads and this is where we can help you.

      </p>
      <div class="flex justify-center gap-7 sm:flex-col sm:gap-0 sm:mt-4">
        <a
          class="redBtn sm:text-center sm:mt-4 sm:text-base sm:max-w-full "
          href=""
        >
          Start here
        </a>
        <a class="inline-block mt-6 py-[10px] px-8 rounded-2xl
          text-lightred text-xl tracking-tight border-2 border-lightred
          sm:text-center sm:mt-4 sm:text-base"
        >
          Learn more
        </a>
      </div>
    </div>
  </section>
);
