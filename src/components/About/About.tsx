import Team from './assets/team.jpg';

export const About = () => (
  <div class="container">
    <div class="flex justify-between mt-40 pb-32 lg:flex-col lg:mt-32 lg:pb-24">
      <div class="flex flex-col w-1/2 ">
        <div className="text-darkred font-bold text-xl tracking-tight">about us</div>
        <h2 class="mt-[2px] text-4xl font-black lg:text-3xl sm:text-2xl">The team behind the brand</h2>
        <p class="mt-[10px] max-w-sm text-lg lg:max-w-full sm:text-base">
          Proin elementum fermentum auctor.
          Nulla semper, est eget congue pellentesque, erat nulla molestie mi, in finibus leo nisl ac lectus. Praesent non urna.
          Nulla congue porta lectus in laoreet. Aenean pellentesque vitae metus id porttitor
        </p>
        <a href="" class="redBtn sm:max-w-full sm:max-w-full sm:text-center">Learn more</a>

        <div class="flex flex-col w-[450px] lg:mt-6">
          <img src={Team} alt="" />
        </div>
      </div>
    </div>
  </div>
);
